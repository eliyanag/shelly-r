#����� ����� �������
train <- read.csv("train.csv", sep=",")
test <- read.csv("test.csv", sep=",")
str(train)
str(test)

#Q1 - Analyze the correlation among the attributes 
any(is.na(train))
any(is.na(test))

nrow(train)
nrow(test)

cols.numeric.train <- sapply(train,is.numeric)
train.numeric <- train[,cols.numeric.train]
cols.numeric.test <- sapply(test,is.numeric)
test.numeric <- test[,cols.numeric.test]

cor.data.train <- cor(train.numeric)
cor.data.test <- cor(test.numeric)

library(corrplot)
print(corrplot(cor.data.train, method = 'color')) 
print(corrplot(cor.data.test, method = 'color')) 

#Q2 - Produce the linear model on the training set 
library(caTools)
set.seed(101)
sample <- sample.split(train.numeric$count, SplitRatio = 0.7)

trainModel <- subset(train.numeric, sample)
testModel <- subset(train.numeric, !sample)

nrow(train)
nrow(trainModel)
nrow(testModel)
nrow(trainModel) + nrow(testModel)
model <- lm(count ~ ., data = trainModel)
print(summary(model))
res <- residuals(model)
hist(res, col = 'blue')

#Q3 - Test the model on the test set 
count.predicted <- predict(model,testModel)
#computing error on the test set
results <- cbind(count.predicted, testModel$count)
colnames(results) <- c('Predicted', 'Actual')

results <- as.data.frame(results) 
to_zero <- function(x){
  if(x<0){
    return (0)
  } else {
    return (x)
  }
}
results$Predicted <- sapply(results$Predicted,to_zero)

min(results$Predicted)
MSE <- mean((results$Actual-results$Predicted)^2)
RMSE <- MSE^0.5

count.predicted.train <- predict(model,trainModel)
results.train <- cbind(count.predicted.train, trainModel$count)
colnames(results.train) <- c('Predicted', 'Actual')
results.train <- as.data.frame(results.train)
results.train$Predicted <- sapply(results.train$Predicted,to_zero)

MSE.train <- mean((results.train$Actual-results.train$Predicted)^2)
RMSE.train <- MSE.train^0.5
#�� ����� ������ ������ ����� ���� ����� ���� �� ����� �� ���� ������
#Q4 - Test for overfitting 
overfitting <- ((RMSE - RMSE.train)/RMSE)*100
